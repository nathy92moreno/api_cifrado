"""Este módulo realiza el cifrado cesar"""

ALFABETO =  "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"


def cifrado_cesar(mensaje,clave):
    """Esta función genera un cifrado cesar a partir de un mensaje

    Args:
        mensaje (str): mensaje a ser encriptado
        clave (int): clave de cifrado
    Return: 
        texto_cifrado: texto cifrado o codificado
        """
    texto_cifrado = ""
    for letra in mensaje.upper():
        if letra in ALFABETO:
            posicion = ALFABETO.index(letra)
            nueva_posicion = (posicion + clave) % len(ALFABETO)
            nueva_letra = ALFABETO[nueva_posicion]
            texto_cifrado += nueva_letra
        else:
            texto_cifrado += letra
    return texto_cifrado
